# Intro Thoughts on NoSQL

At the highest possible level, RDBMS and NoSQL solutions are just ways to store data.

RDBMS solutions rely on the a single server as its source of truth.

## Querying NoSQL

### Key-Value Store

Most NoSQL databases are **key-value stores**. This means that only two queries are supported by default: (1) select all objects of a given type and (2) select an object with a given ID. That's pretty boring.

### Understanding Map-Reduce

A NoSQL map is comparable to an RDBMS view. One or more columns from one or more data sets can be combined to produce a new data set. The difference is that the map concept is designed to be distributed, cached, and batched across multiple servers.

* Wikipedia: [MapReduce](http://en.wikipedia.org/wiki/MapReduce)
* Ayende: [Map/Reduce: A Visual Explanation](http://ayende.com/blog/4435/map-reduce-a-visual-explanation)

## Cassandra

### Background

[Cassandra](http://cassandra.apache.org/) was originally developed by Facebook. It was open-sourced and adopted by the Apache project.

### Query style

[Cassandra Query Langauge](http://cassandra.apache.org/doc/cql/CQL.html)

### Data Consistency

**Write to quorum. Read from quorum. Eventually consistent.**

All writes in Cassandra are timestamped, and Cassandra uses the concept of quorum to reinforce concurrency. *Quorum = 50% + 1 servers.* By reinforcing quorum reads and write, retrieving stale data is *generally* prevented. "Eventually" all data is replicated to all servers in the cluster. Every server will eventually have version `T2` of the data. This example assumes that two writes and one read take place before consistency is achieved. This replication process varies based on the amount of data, number of servers, and amount of network traffic. The total time can be anywhere from a few ms to a several minutes.

| Server | Initial State | Write   | Write   | Read   | Repl   |
|--------|---------------|---------|---------|--------|--------|
| S1     | *null*        | **T1**  | T1      | T1     | **T2** |
| S2     | *null*        | **T1**  | **T2**  |        | **T2** |
| S3     | *null*        | **T1**  | **T2**  |        | **T2** |
| S4     | *null*        | *null*  | **T2**  | **T2** | **T2** |
| S5     | *null*        | *null*  | *null*  | *null* | **T2** |

In the above example, the data from server **S4**, timestamp `T2`, would be recognized as the newest data. Servers are periodically polled for availability. If a new server were added to the cluster, the value for `Q` would be incremented to 4 (`6 / 2 + 1 = 4`). During the next replication event, all data would be copied to the newest server.

### Query Aggregation

The current version of Cassandra, 1.2.3, does not support query aggregation. It is not possible to perform the following query with Cassandra.

    select count(*) from orders where order_year = 2013 and order_month = 1 and order_amount > 1000.00

This type of query is not possible because there is no master server in Cassandra

## Riak

### Background

Riak was written by [Basho](http://basho.com/) based on the concepts of Amazon's Dynamo.

* Wikipedia: [Riak]()

### Query Style

[Map-Reduce](http://docs.basho.com/riak/1.1.4/tutorials/fast-track/Loading-Data-and-Running-MapReduce-Queries/) with a JavaScript API over HTTP.

### Data Consistency

**Write to 3. Read from all. Eventually consistent.**

All writes in Riak are timestamped and replicated to three (3) servers. All reads are distributed to all servers. Like in Cassandra, "eventually" all data is replicated to all servers in the cluster. Again, this timeframe may be from a few ms to several minutes.

| Server | Initial State | Write   | Write   | Read   | Repl   |
|--------|---------------|---------|---------|--------|--------|
| S1     | *null*        | **T1**  | T1      | T1     | **T2** |
| S2     | *null*        | **T1**  | T1      | T1     | **T2** |
| S3     | *null*        | **T1**  | **T2**  | **T2** | **T2** |
| S4     | *null*        | *null*  | **T2**  | **T2** | **T2** |
| S5     | *null*        | *null*  | **T2**  | **T2** | **T2** |
| S6     | *null*        | *null*  | *null*  | *null* | **T2** |
| S7     | *null*        | *null*  | *null*  | *null* | **T2** |
| S8     | *null*        | *null*  | *null*  | *null* | **T2** |

### Query Aggregation

Riak's JavaScript API allows for aggregate queries. As part of the query, you can specify a function to execute. The actual MapReduce is written in [Erlang](http://www.erlang.org/).
