NHibernate does a lot of its own internal caching. This demonstrates some of the types of query caching NHibernate will perform.

## The Data

**Posts**

| ID | Author  | Content |
|----|---------|---------|
| 1  | Jarrett | ...     |
| 2  | Jamie   | ...     |
| 3  | Doug    | ...     |

**Comments**

| ID | Author  | Post_id | Moderated | Content |
|----|---------|---------|-----------|---------|
| 1  | Annie   | 1       | T         | ...     |
| 2  | Bob     | 2       | T         | ...     |
| 3  | Charlie | 1       | F         | ...     |

## The Code

### Scenario 1

    // Fetch post with ID = 1
    //
    // select * from Posts where ID = 1
    Post post = session.Load<Post>(1);

    // Get comments for the post
    //
    // select * from Comments where Post_id = 1
    IEnumerable<Comment> comments = post.Comments;

    // Count the number of posts.
    //
    // select count(*) from Posts
    int postCount = session.Query<Post>().Count();

    // Count the comments for post 1.
    //
    // The comments for the post have already been loaded into
    // local memory, so a new query will not be sent to the 
    // database.
    int commentCount = post.Comments.Count();

**Total number of queries:** 3.

### Scenario 2

This time, we change the order that we're doing work.

    // Fetch post with ID = 1
    //
    // select * from Posts where ID = 1
    Post post = session.Load<Post>(1);

    // Count the comments for post 1.
    //
    // Comments have not yet been loaded, so we'll send a count
    // query to the database.
    //
    // select count(*) from Comments where Post_id = 1
    int commentCount = post.Comments.Count();

    // Get comments for the post
    //
    // We've loaded the count in the previous query, but not
    // the actual comments, so NHibernate knows it needs to go 
    // to the server again.
    //
    // select * from Comments where Post_id = 1
    IEnumerable<Comment> comments = post.Comments;

    // Count the number of posts.
    //
    // select count(*) from Posts
    int postCount = session.Query<Post>().Count();

**Total number of queries:** 4.

### Scenario 3

This time, we'll load up all the posts first.

    // Fetch all posts.
    //
    // select * from Posts
    IEnumerable<Post> posts = session.Query<Post>();

    // Load post #1.
    //
    // No query is sent to the database because Post 1 is 
    // already in local memory.
    Post post = session.Load<Post>(1);

    // Count the number of posts.
    //
    // Again, no query is sent, since all posts are in
    // local memory.
    int postCount = session.Query<Post>().Count();

    // Select the comments for Post #1.
    //
    // select * from Comments where Post_id = 1
    IEnumerable<Comment> comments = post.Comments;

**Total number of queries:** 2.

### Scenario 4

Load everything into memory in a single query.

    // Select all posts and comments in a single query.
    // 
    // select t0.*, t1.* from Posts t0 inner join Comments t1 on t0.ID = t1.Post_id
    IEnumerable<Post> posts = session.Query<Post>().FetchMany<Comment>();

    // Fetch Post #1.
    // 
    // Already loaded. No query.
    Post post = session.Load<Post>(1);

    // Select the comments for the post.
    // 
    // Already loaded. No query.
    IEnumerable<Comment> comments = post.Comments;

**Total number of queries:** 1.

### Scenario 5

More counting examples

    // Load post #1.
    //
    Post post = session.Load<Post>(1);

    // Count the number of moderated comments.
    //
    // select count(*) from Comments where Post_id = 1 and Moderated = 'T'
    int moderatedCount = post.Comments.Where(c => c.Moderated).Count();

    // Count the total number of comments.
    //
    // select count(*) from Comments where Post_id = 1
    int commentCount = post.Comments.Count();

