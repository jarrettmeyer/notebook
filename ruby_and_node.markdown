> On Mar 22, 2013, at 10:47 PM, "Kurtz, Jamie" <jkurtz@fusionalliance.com> wrote:
> 
> I'm thinking about switching the backend of my web site / REST service to nodejs - probably still running on Heroku. I haven't invested much into Ruby yet, and I feel like learning javascript will better serve me in the long run - especially in the world of mostly Windows-related development.
> 
> Any thoughts on that?
> 
> Obviously, I'm not looking for an over-generalized "node is better" statement. More just your thoughts on learning node, ruby versus node on Windows 8, node as a skillset, node as a forward technology, node at Fusion and our customers versus Ruby at Fusion and our customers, etc.

## Your Application

Do both! It's a learning thing. See which one you like better. You have similar components in each case. It's worth noting that in both cases, you never really need to concern yourself with the middleware. The platform just takes care of it all.

|          |Option #1|Option #2 |
|----------|---------|----------|
|Language  |Ruby     |JavaScript|
|Platform  |Sinatra  |Express   |
|Middleware|Rack     |Node/HTTP |

## The Fusion Question

It depends.

Okay, I'll go into a deeper answer. Prepare for a wall of text.

## Learning JavaScript

JavaScript is not an optional language. Every web page has at least a little. And JavaScript sucks. It's really a terrible language, it gets treated as a copy-paste language, so learning to write it well is the only option. If they haven't done so, every developer should immediately go out and read [JavaScript: The Good Parts](http://www.amazon.com/JavaScript-The-Good-Parts-ebook/dp/B0026OR2ZY/ref=sr_1_2?ie=UTF8&qid=1364127697&sr=8-2&keywords=javascript%3A+the+good+parts) by Douglas Crawford.

Every JavaScript author should understand global scope, closure, implicit variable definition, variable hiding, `this`, and prototypical objects. They should understand the difference between truthy and falsy, as opposed to C#'s strict `true` and `false`. They should understand the difference between `==` and `===`.

On Windows 8, you can write Metro Apps entirely in HTML and JavaScript. Imagine a browser app without the browser chrome. You don't have to use WPF.

## Learning Ruby

For me, Ruby is my go-to, get shit done language. I have yet to find another language that can do so much, is so easy to read, and has so little character noise for so few keystrokes.

But, that's okay, because [Ruby isn't cool any more](http://www.codinghorror.com/blog/2013/03/why-ruby.html). All the cool kids are learning Scala and Node.

The concern we have is that Ruby, as created by Matz, was built from the beginning to combine Perl, the Unix/Linux command line, Lisp, Smalltalk, and CLU. Then, in the one thing that makes Ruby stand out from almost every other language, do all of this to make the language incredibly human friendly, instead of compiler or interpreter friendly.

## Scripting Is Important

Personally, I don't think it's important that developers learn Ruby. However, I think it's incredibly important that **every Fusion developer learn a scripting language**.

Think about the following one-off, every day kinds of tasks we do.

> Once per week, take all files in the `/log` directory, compress them (`zip` on Windows, `gz` on Linux), save the file as log-archive-yyyy-mm-dd-hh-mm-ss, and move put it in `/log/archives`.

Or how about this one?

> Take a backup of the CCC database. Save the backup locally in your `/var/postgresql/backups` folder. Save the backup as `ccc_production_yyyy_mm_dd_hh_mm_ss.backup`. Upload the file to Amazon S3, saving the file in a `pgbackups` container. Delete any local backups older than three days. Set up `cron` to run once per day.

For me, these are Ruby scripts. Since `pg_dump` is a simple command line call, Ruby makes it ridiculously easy to get to the command line, and uploading a file to S3 when using the `AWS/S3` gem is a one liner, that last one is only 8 lines long.

How many of our developers would open Visual Studio and create a console application for that kind of crap? Oh, and you'll have to worry about non-GAC'ed DLLs for the S3 Nuget package, blah, blah, blah. It's just so much crap, and I am done with it. I don't want it in my life any longer. Give me a Linux command line and get out of my way. Seriously, I can't go back to Microsoft development, but that's another issue for another (hopefully much later) time.

I don't really think it's important what scripting language you learn. Ruby, Python, NodeJS, and even PowerShell fit this bill. Although PowerShell still has the *how do I find a non-GAC DLLs* problem mentioned above.

## Learning CoffeeScript

I'm excited about [CoffeeScript](http://coffeescript.org/). It gets rid of a lot of the character noise in JavaScript. It adds tons of helpers to the language: conditional assignment, an `unless` keyword, implicit null checks, automatically converts all `==` to `===`, a `for`-`in` syntax that actually make sense, etc.

CoffeeScript reinforces the **Pit of Success** by making it impossible to do bad things. If you do things the compiler doesn't recognize, it won't be able to compile your `*.coffee` file into a `*.js` file. In fact, I've gone back to a lot of my stuff and found bad JavaScript code by trying to convert it to CoffeeScript and failing. I then get it working in CoffeeScript and look at the resulting JavaScript. I then have one of those, "Oh, that's what it should look like," moments.

I like to use the phrase, "I can write bad in any language." I'm starting to wonder if that's actually true in CoffeeScript.

Dropbox.com [rewrote their JavaScript to CoffeeScript](https://tech.dropbox.com/2012/09/dropbox-dives-into-coffeescript/) last year. I'll summarize, if you don't care to read the original post.

* Before: 23,000 lines of JavaScript
* After: 18,000 lines of CoffeeScript
* The commenters fall into two camps
  * Pro-CoffeeScript: Less, more readable code
  * Pro-JavaScript: Your JavaScript examples suck

Refer to my comments about the Pit of Success. The commenters who are pro-JavaScript point out the flaws in the blog's example JavaScript code, but do nothing to address the whole "less, more readable code" argument.

Finally, go look at the [Node Package Manager](https://npmjs.org/). About half of the user-submitted packages are written in CoffeeScript.

Finally, Node is quite intelligent. You don't even have to compile your CoffeeScript first. Simply `npm install -g coffee-script` and your CoffeeScript files will work natively.

## Ruby on Rails

In my opinion, Ruby on Rails is still the the gateway to the non-Microsoft world. Most of us are used to working on big applications. Node does not have an equivalent platform. (Node on Rails?) If you think about your "typical" MSMVC application, you have multiple controller, models, database calls, lots of assets (images, CSS, JS), service classes, etc. Anything you can write in MSMVC, you can also write in Ruby on Rails. Hopefully, any such exercise would demonstrate just how much the Rails framework does for you. Things that used to be a problem in MSMVC are gone in Rails.

You are not going to write a CMS on Node.

### Developing Ruby on Rails on Windows

This has got some problems. I've run into them, and learned my way around them. Some are fixable; some are not.

* **It's slow.** The [MingW Ruby interpretter](http://rubyinstaller.org/) for Windows is just God-awful slow on equal hardware. This is because the MingW interpreter has to patch in replacements for all of the stuff that isn't there: `awk`, `curl`, `grep`, `sed`, `wget`, etc.

* **The debugger doesn't work.** Personally, I think using the debugger is a failure as a developer, but there are times when you still have to do it. Unfortunately, the debugger in Rails uses `libreadline`.

* **Testing SSL doesn't work.** Rails' SSL depends on `openssl`. Once again, this does not exist on Windows. Developing and testing your application under SSL does not work.

* **It's not Visual Studio.** Yes, I have worked with developers who are confused by having a text editor with basic functionality and syntax highlighting. Most Ruby developers are using [TextMate](http://macromates.com/), [Sublime](http://www.sublimetext.com/), [RubyMine](http://www.jetbrains.com/ruby/) or VIM. TextMate is Mac only.

* **Compiled Gem support is a day late.** Any Gems that have to be compiled locally are usually behind. Plus, Windows devs just aren't used to `gcc`, `make`, etc. You need to install these tools, usually as part of the [Ruby DevKit](https://github.com/oneclick/rubyinstaller/wiki/development-kit) to get everything working correctly.

### Deploying Ruby on Rails on Windows Server

Don't even try.

If you have a requirement that states, "The application must run on our existing Windows Servers," then you should immediately take Ruby on Rails off the table. You are missing too much. [RVM](https://rvm.io/) doesn't work. [Capistrano](https://github.com/capistrano/capistrano) doesn't work. [Passenger](https://www.phusionpassenger.com/) doesn't work.

Plus, deployment is usually done through SSH keys. Windows makes a terrible SSH server. On Debian, you can `sudo apt-get install openssh-server` and you're ready to start deploying Ruby applications.

## Express vs. Sinatra

These two development environments, as far as I can tell, as equivalent. So we're back to the common themes previously dicussed.

* Every (web) developer must learn JavaScript. Every JavaScript developer should learn JavaScript well.
* NodeJS strives to be the same on all platforms. When you go to [NodeJS.org](http://nodejs.org/), all [downloads for all platforms are available](http://nodejs.org/download/).
* When you got to [Ruby-Lang.org](http://www.ruby-lang.org/), they send you to a completely different site operated by completely different people for the Windows stuff.
* Ruby has the Unix/Linux command line versatility as a fundamental design concern.
* Ruby, in my opinion, is the most readable language ever.
* CoffeeScript is JavaScript + Awesome. It makes JavaScript as readable as Ruby and prevents you from doing stupid, unintended, or just plain wrong stuff.
* Node does not have a large application framework like Rails. So how big would you let your Express or Sinatra application get before you want a bigger framework?
* Node has native, implicit background threads, but you can't get to them. The only thread you can ever access is the invocation thread. If you must have multi-threading, you can't use Node.

## Other Thoughts on Node

Node is, [according to their blog](http://blog.nodejs.org/2013/03/11/node-v0-10-0-stable/), 2 stable releases from 1.0. Ruby just released version 2.0. The Rails team is close to shipping version 4.0 of their framework. There's something to be said for maturity. I don't know what that is though, since I frown on personal maturity.

## Mixing of Opinions

### Jarrett Meyer

If I were me, I'd never work on Windows again. Windows gets in my way of being productive. Even if I'm writing a Node application for Windows, I'd author on my laptop and send you the code with a strong amount of confidence that it will work the same at a customer's site.

### Jamie Kurtz

*I apologize for putting words in your mouth. I'm trying to figure out how you're going to process all of this.*

You don't have the luxury of my completely one-sided opinion. For mixed-platform use, all options are pointing to Node/Express. You're capped by the size of the program.

## One Final Caveat

Look to [Amazon.com](http://www.amazon.com). They use Node, and their site is bigger than huge. But they've crafted their site differently. Their site is composed from dozens (hundreds?) of independent calls. Open up Chrome's networking tools, clear your cache and look at how many server calls you have.

By keeping the independent components small, you don't have one giant Amazon.com application. You have hundreds of little applications that do one thing and then get composed by a wrapper. Even the wrapper itself is "dumb". It just knows what servers to call and dumps the content to the screen. I've been told that the Amazon.com wrapper isn't even a compiled application. It's XML + XSLT that gets donut-cached into HTML.

When you think about writing a composed application, it eliminates the lack of a large framework from being a problem.